# How to deploy Nørrebro.space

0: Make sure you have credentials for stuff.

1: Manually provision a server running Debian 10.

- You need to set up ssh for the bootstrapping user manually.
- Set *. and @. DNS records
  * `@                   A      $IPv4`
  * `@                   AAAA   $IPv6`
  * `@                   TXT    v=spf1 include:mailbox.org ~all`
  * `_dmarc              TXT    v=DMARC1; p=quarantine;`
  * `MBO0001._domainkey  CNAME  MBO0001._domainkey.mailbox.org.`
  * `MBO0002._domainkey  CNAME  MBO0002._domainkey.mailbox.org.`
  * `MBO0003._domainkey  CNAME  MBO0003._domainkey.mailbox.org.`
  * `MBO0004._domainkey  CNAME  MBO0004._domainkey.mailbox.org.`
- Set DMARC and SPF to reject mails from domains that shouldn't send emails (`nørrebro.space`)
  * `@                   TXT    v=spf1 -all`
  * `_dmarc              TXT    v=DMARC1; p=reject;`
- Attach a block storage volume to `/dev/sdb`

2: Make sure dependencies are installed.

You need:
  - ansible (python3+ !!!)
  - ansible-galaxy
  - ansible-galaxy packages:
    - geerlingguy.docker
    - jnv.unattended-upgrades


3: Run the playbook

`make deploy`


4: (Only on first setup) Initialize the database

This step only needs to be done once, to initalize a new database.

Once the playbook has run successfully,
ssh to the server and run the following one-time commands:


```
# TODO: I am an idiot. make bootstrap playbook
# stop mastodon containers
docker stop web streaming sidekiq 
# initialize database
sudo docker run \
  --env-file /mnt/data/mastodon.env \
  --network internal_network \
  -v "/mnt/data/mastodon:/mastodon/public/system:rw" \
  -it "tootsuite/mastodon" rails db:migrate
# precompile assets
sudo docker run \
  --env-file /mnt/data/mastodon.env \
  --network internal_network \
  -v "/mnt/data/mastodon:/mastodon/public/system:rw" \
  -it "tootsuite/mastodon" rails assets:precompile
# start containers again
docker start web streaming sidekiq
```

5: Toot!


## Tips

This guide seems really comprehensive on masto admin stuff:

https://www.howtoforge.com/how-to-install-mastodon-social-network-with-docker-on-ubuntu-1804/
